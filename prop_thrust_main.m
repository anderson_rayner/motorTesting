%% Main script for prop thrust measurements
try; fclose(instrfindall); end;
clear all
clc

tic

%% Inputs
COM_PORT = 'COM28';                  % Change to whatever COM port the arduino is on
PWMs = 1800:-50:900;

% Prop/Motor/ESC data
motor.kV = 2900;
motor.name = 'Turnigy 2211-2900kV';
prop.D = 4;                          % Propeller diameter [ in ]
prop.P = 2.5;                        % Propeller pitch    [ in ]
esc.name = 'Turnigy Plush 6A';

% Environmental
T = 23.2;             % Air temperature [ C ]
P = 101300;           % Air pressure    [ Pa ]

%% Calculate constants
R = 287.05;
rho = P/(R*(T+273.15));

%% Start
fprintf('Motor Thrust and Power Script\n');
fprintf('=============================\n');

%% Set up figures
figure(1); clf; hold all; set(1,'name','Raw Data');
figure(2); clf; hold all; set(2,'name','Trend Fits');

%% Open serial port
serial_port = serial(COM_PORT,'BaudRate',115200,'InputBufferSize',4096);
fopen(serial_port);

%% Initialise NIDAQ
if (~isempty(daqfind))
    stop(daqfind)
end

s = daq.createSession('ni');

%% Configure session properties
s.DurationInSeconds = 2.0;
s.Rate = 2000;

%% Add channels to the session
% Loadcell
addAnalogInputChannel(s,'Dev2',0:5,'Voltage');

% Volt/Amps
addAnalogInputChannel(s,'Dev2',6:7,'Voltage');

%% Bias loadcell
data = startForeground(s);
bias = mean(data(:,1:6));
fprintf('Bias values are %6.2f, %6.2f, %6.2f, %6.2f, %6.2f, %6.2f [ V ]\n',bias)

fprintf('Loadcell biased.  Press any key to continue\n');

pause;

%% Initialise Motor
fprintf(serial_port,'%d\n',900);
pause(2);

% Turn to idle
fprintf(serial_port,'%d\n',1100);
pause(2);

% Ramp motor
PWM_ramp = linspace(1100,PWMs(1),20);
for ii = 1:length(PWM_ramp)
    fprintf(serial_port,'%d\n',floor(PWM_ramp(ii)));
    pause(0.1);
end

%% Loop through each of the PWMs
PWM     = nan(size(PWMs'));
Thrust  = nan(size(PWMs'));
Torque  = nan(size(PWMs'));
Voltage = nan(size(PWMs'));
Current = nan(size(PWMs'));
Power   = nan(size(PWMs'));
RPM     = nan(size(PWMs'));
fprintf('\n\n');

for ii = 1:length(PWMs)
    fprintf('Testing PWM %4d\n',PWMs(ii));
    
    % Send new PWM to system
    fprintf(serial_port,'%d\n',PWMs(ii));
    
    % Wait for system to stabilise
    pause(2);
    
    % Flush arduino buffer
    flushinput(serial_port);
    
    % Take reading
    data = startForeground(s);
    data = mean(data);
    
    % Subtract bias from Loadcell readings
    data(1:6) = data(1:6) - bias;
    
    % Convert voltages to forces
    Force = Loadcell_Calibration(data(1:6),5,1);
    
    % Get data from Arduino
    RPM_temp = [];
    while serial_port.BytesAvailable >= 10
        xx = fgetl(serial_port);
        yy = sscanf(xx,'%d');
        
        if length(yy) == 1
            RPM_temp(end+1) = yy;
        end
        
        % If we have 1000 data points it should be enough
        if length(RPM_temp) >= 100
            flushinput(serial_port)
        end
    end
    
    RPM_in = mean(RPM_temp);
    
    % Save values
    PWM(ii)    = PWMs(ii);
    Thrust(ii) = norm([Force.X,Force.Y,Force.Z]);
    Torque(ii) = abs(Force.N);
    Voltage(ii) = data(7)*1.9826;   % Calibration for 2S, 10A sensor
    Current(ii) = data(8)*1.4980;   % Calibration for 2S, 10A sensor
    Power(ii)   = Voltage(ii)*Current(ii);
    RPM(ii)     = RPM_in;
    
    % Plot
    % Thrust
    figure(1); clf;
    subplot(2,2,1); ...
        plot(PWMs,Thrust,'o-'); ...
        ylabel('Thrust [ N ]'); ...
        xlabel('PWM [ \mus]');
    % Torque
    subplot(2,2,2); ...
        plot(PWMs,Torque,'o-'); ...
        ylabel('Torque [ N.m ]'); ...
        xlabel('PWM [ \mus]');
    % RPM
    subplot(2,2,3); ...
        plot(PWMs,RPM,'o-'); ...
        ylabel('RPM [ - ]'); ...
        xlabel('PWM [ \mus]');
    % Power
    subplot(2,2,4); ...
        plot(PWMs,Power,'o-'); ...
        ylabel('Power [ W ]'); ...
        xlabel('PWM [ \mus]');
    
    % Print to terminal
    fprintf('     RPM : %6d\n',floor(RPM(ii)));
    fprintf('    Volt : %6.2f\n',Voltage(ii));
    fprintf('     Amp : %6.2f\n',Current(ii));
    
end

fclose(serial_port);

%% Save data
fprintf('Saving data\n');
if ~exist('./data/','dir'); mkdir('./data'); end
filename = ['./data/Motor Data - ',datestr(now),'.mat'];
filename(filename==':') = '.';
save(filename,'PWM','Thrust','Torque','Voltage','Current','Power','RPM',...
    'motor','esc','prop','rho');
fprintf('\n\nFinished.  Time taken %.2f s!\n',toc);

%% Quickly calculate CT, CQ, CP and stuff
D = prop.D*0.0254;
locs = ~isnan(RPM);
n = RPM(locs)/60; n_fit = linspace(min(n),max(n));
CT_poly = polyfit(n.^2,Thrust(locs),1); ...
    CT = CT_poly(1)/(rho*D^4);
CQ_poly = polyfit(n.^2,Torque(locs),1); ...
    CQ = CQ_poly(1)/(rho*D^5);
CP_poly = polyfit(n.^3,Power(locs) ,1); ...
    CP = CP_poly(1)/(rho*D^5);  c_Power = CP_poly(2);

figure(2); clf; hold all;
subplot(2,2,1); hold all; ...
    plot(n_fit.^2,CT*rho*D^4*n_fit.^2+CT_poly(2)); ...
    plot(n.^2,Thrust(locs),'or'); ...
    ylabel('Thrust [ N ]'); xlabel('n^2'); ...
    legend(['CT: ',num2str(CT,'%.4f')],'Data','location','northWest');
subplot(2,2,2); hold all; ...
    plot(n_fit.^2,CQ*rho*D^5*n_fit.^2+CQ_poly(2)); ...
    plot(n.^2,Torque(locs),'or'); ...
    ylabel('Torque [ N.m ]'); xlabel('n^2'); ...
    legend(['CQ: ',num2str(CQ,'%.4f')],'Data','location','northWest');
subplot(2,2,3); hold all; ...
    plot(n_fit.^3,CP*rho*D^5*n_fit.^3+c_Power); ...
    plot(n.^3,Power(locs),'or'); ...
    ylabel('Power [ W ]'); xlabel('n^3'); ...
    legend(['CP: ',num2str(CP,'%.4f'),', c\_Watts: ',num2str(c_Power,'%.2f')],'Data','location','northWest');
subplot(2,2,4); hold all; ...
    plot(Power,Thrust,'o-');
    ylabel('Thrust [ N ]'); xlabel('Power [ W ]');

fprintf('\nResults\n');
fprintf('  CT = %.4f [ - ]\n',CT);
fprintf('  CQ = %.4f [ - ]\n',CQ);
fprintf('  CP = %.4f [ - ]  c_Power = %.4f [ W ]\n',CP,c_Power);

return
